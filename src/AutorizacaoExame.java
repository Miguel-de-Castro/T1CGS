import java.util.Date;
import java.util.ArrayList;
import java.util.Scanner;

public class AutorizacaoExame {

    //atributos e construtor
    private int codAut;
    private Date dataCadastro;
    private Medico medicoSoli;
    private Paciente paciente;
    private Exame exameSoli;

    static private ArrayList<AutorizacaoExame> listaAutorizacoes = new ArrayList<>();

    Scanner sc = new Scanner(System.in);

    public AutorizacaoExame() {

    }

    public AutorizacaoExame(Date dataCadastro, Medico medicoSoli, Paciente paciente, Exame exameSoli) {
        this.codAut = (int) System.currentTimeMillis();
        this.dataCadastro = dataCadastro;
        this.medicoSoli = medicoSoli;
        this.paciente = paciente;
        this.exameSoli = exameSoli;
    }

    //metodos principais

    //metodos especiais
    public int getCodAut() {
        return codAut;
    }

    public void setCodAut(int codAut) {
        this.codAut = codAut;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Medico getMedicoSoli() {
        return medicoSoli;
    }

    public void setMedicoSoli(Medico medicoSoli) {
        this.medicoSoli = medicoSoli;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Exame getExameSoli() {
        return exameSoli;
    }

    public void setExameSoli(Exame exameSoli) {
        this.exameSoli = exameSoli;
    }

    public static ArrayList<AutorizacaoExame> getListaAutorizacoes() {
        return listaAutorizacoes;
    }

    @Override
    public String toString() {
        return "{" +
            " codAut='" + getCodAut() + "'" +
            ", dataCadastro='" + getDataCadastro() + "'" +
            ", medicoSoli='" + getMedicoSoli() + "'" +
            ", paciente='" + getPaciente() + "'" +
            ", exameSoli='" + getExameSoli() + "'" +
            "}";
    }
    
}