import java.util.HashMap;
import java.util.Map;

public enum Cirurgia {

    Emergência("Emergência"),
    Paliativa("Paliativa"),
    Reconstrutora("Reconstrutora ");
    private final String label;

    private Cirurgia(String label){
        this.label = label;
    }

    public static Cirurgia valueOfLabel(String label) {
        for (Cirurgia e : values()) {
            if (e.label.equals(label)) {
                return e;
            }
        }
        return null;
    }

    public String getLabel() {
        return label;
    }
}
