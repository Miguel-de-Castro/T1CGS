public enum TipoExames {
    Tomografia,
    RaioX,
    Hemograma,
    RM,
    Endoscopia,
    Biópsia,
    Ultrassom,
    Colesterol,
    Glicemia,
    Angiografia
}
